use ipnetwork::IpNetwork;
use rangemap::RangeInclusiveMap;

use std::fs::File;
use std::io::{BufRead, BufReader, Write};
use std::net::{IpAddr, Ipv6Addr};
use std::convert::TryInto;

use std::collections::{HashMap,BTreeSet};

#[derive(Debug, Clone)]
pub struct NetBlock {
    pub net: IpNetwork,
    pub cc: CCode,
}

type CCode = [u8;2];
type AsnMap = HashMap<u32,CCode>;

fn read_asn_file() -> anyhow::Result<AsnMap> {
    let f = File::open("../data-used-autnums")?;
    let f = BufReader::new(f);

    let mut map = AsnMap::new();

    for linebuf in f.lines() {
        let line = linebuf?;
        let line = line.trim();
        let asn = line.splitn(2, ' ').next().unwrap();
        let end = &line[line.len() - 4..];

        if ! end.starts_with(", ") {
            continue;
        }

        map.insert(asn.parse()?, end[2..].as_bytes().try_into().unwrap());
    }

    Ok(map)
}

impl PartialEq for NetBlock {
    fn eq(&self, other: &Self) -> bool {
        self.net == other.net
    }
}
impl Ord for NetBlock {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.net
            .network()
            .cmp(&other.net.network())
            .then_with(|| self.net.prefix().cmp(&other.net.prefix()))
    }
}

impl PartialOrd for NetBlock {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for NetBlock {}

fn main() -> anyhow::Result<()> {
    let asns = read_asn_file()?;

    let f = File::open("../data-raw-table")?;
    let f = BufReader::new(f);

    let mut v4map: RangeInclusiveMap<u32, CCode, _> = RangeInclusiveMap::new();

    for linebuf in f.lines() {
        let line = linebuf?;
        let items : Vec<_> = line.split_whitespace().collect();
        let nb: IpNetwork = items[0].parse()?;
        let asn: u32 = items[1].parse()?;
        let cc = asns.get(&asn).unwrap_or(b"??".into());

        let start = nb.network();
        let end = nb.broadcast();
        match (start, end) {
            (IpAddr::V4(a), IpAddr::V4(b)) => {
                v4map.insert(a.into()..=b.into(), *cc);
            }
            /*
            (IpAddr::V6(a), IpAddr::V6(b)) => {
                v6map.insert(a.into()..=b.into(), cc);
            }
            */
            (_, _) => panic!(),
        }
    }

    let mut v4 = File::create("geoip")?;

    for (r, cc) in v4map.iter() {
        writeln!(
            &mut v4,
            "{},{},{}",
            r.start(),
            r.end(),
            std::str::from_utf8(cc).unwrap()
        )?;
    }

    Ok(())
}
