#!/usr/bin/python

compare = [ (4,8), (4,10),
            (6,8), (6,10),
            (8,10),
            (4,14), (6,14), (8,14),
            (10,14)
           ]
compare_v6 = [ (5,9), (5,11),
               (7,9), (7,11),
               (9,11)
              ]


res = { c:0 for c in compare + compare_v6 }
total_v4 = 0
total_v6 = 0

lines = iter(open("comparison.csv"))
headers = next(lines).strip().split(",")
for line in lines:
    cols = line.strip().split(",")
    print(len(cols),cols)
    total_v4 += 1
    for (a,b) in compare:
        if cols[a] == cols[b]:
            res[(a,b)] += 1
    if cols[3]:
        total_v6 += 1
        for (a,b) in compare_v6:
            if cols[a] == cols[b]:
                res[(a,b)] += 1

import pprint
pprint.pprint(res)

for (a,b) in compare:
    print("{} matches {} {}% of the time".format(
        headers[a], headers[b],
        int(res[(a,b)] / total_v4 * 100)))

for (a,b) in compare_v6:
    print("{} matches {} {}% of the time".format(
        headers[a], headers[b],
        int(res[(a,b)] / total_v6 * 100)))

