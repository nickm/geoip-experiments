mod db;

use ipnetwork::IpNetwork;
use rangemap::RangeInclusiveMap;

use std::fs::File;
use std::io::{BufRead, BufReader, Write, BufWriter};
use std::net::{IpAddr, Ipv6Addr};

#[derive(Debug, Clone)]
pub struct NetBlock {
    pub net: IpNetwork,
    pub cc: [u8; 2],
    pub is_anon_proxy: bool,
    pub is_anycast: bool,
    pub is_satellite: bool,
}

impl PartialEq for NetBlock {
    fn eq(&self, other: &Self) -> bool {
        self.net == other.net
    }
}
impl Ord for NetBlock {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.net
            .network()
            .cmp(&other.net.network())
            .then_with(|| self.net.prefix().cmp(&other.net.prefix()))
    }
}

impl PartialOrd for NetBlock {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for NetBlock {}

fn main() -> std::io::Result<()> {
    let f = File::open("database.txt")?;
    let f = BufReader::new(f);
    //let mut blocks = BTreeSet::new();
    let mut blocks = Vec::new();
    // read blocks, and sort them by specificity and address.
    for nb in db::BlockReader::new(f.lines()) {
        blocks.push(nb);
    }
    blocks.sort();

    let mut v4map: RangeInclusiveMap<u32, [u8;2], _> = RangeInclusiveMap::new();
    let mut v6map: RangeInclusiveMap<u128, [u8;2], _> = RangeInclusiveMap::new();
    for nb in blocks.iter() {
        let start = nb.net.network();
        let end = nb.net.broadcast();
        match (start, end) {
            (IpAddr::V4(a), IpAddr::V4(b)) => {
                v4map.insert(a.into()..=b.into(), nb.cc);
            }
            (IpAddr::V6(a), IpAddr::V6(b)) => {
                v6map.insert(a.into()..=b.into(), nb.cc);
            }
            (_, _) => panic!(),
        }
    }

    let mut v4 = BufWriter::new(File::create("ipv4.txt")?);
    let mut v6 = BufWriter::new(File::create("ipv6.txt")?);

    // TODO: This loop and the one below should be turned into common
    // code.

    // XXXX: They are a hackish way to coalesce adjacent
    // ranges, which rangemap is supposed to do for us.  Am I using
    // rangemap wrong?
    let mut p = v4map.iter().peekable();
    while let Some((r, cc)) = p.next() {
        let (start,mut end) = (r.start(), r.end());
        while let Some((rnext, ccnext)) = p.peek() {
            if *ccnext != cc || *end != rnext.start()-1 {
                break
            }
            end = rnext.end();
            let _ = p.next();
        }
        writeln!(
            &mut v4,
            "{},{},{}",
            start,
            end,
            std::str::from_utf8(cc).unwrap()
        )?;
    }

    let mut p = v6map.iter().peekable();
    while let Some((r, cc)) = p.next() {
        let (start,mut end) = (r.start(), r.end());
        while let Some((rnext, ccnext)) = p.peek() {
            if *ccnext != cc || *end != rnext.start()-1 {
                break
            }
            end = rnext.end();
            let _ = p.next();
        }

        let a: Ipv6Addr = (*start).into();
        let b: Ipv6Addr = (*end).into();
        writeln!(
            &mut v6,
            "{},{},{}",
            a,
            b,
            std::str::from_utf8(cc).unwrap()
        )?;
    }

    v4.flush()?;
    v6.flush()?;

    Ok(())
}
