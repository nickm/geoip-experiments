//use std::io::{Lines,BufReader};
use std::collections::HashMap;
use std::convert::TryInto;

use super::NetBlock;

pub struct BlockReader<I>
where
    I: Iterator<Item = std::io::Result<String>>,
{
    iter: I,
}

enum AnyBlock {
    NotNet,
    NetBlock(NetBlock),
}

impl<I> BlockReader<I>
where
    I: Iterator<Item = std::io::Result<String>>,
{
    pub fn new(iter: I) -> Self {
        BlockReader { iter }
    }

    fn get_block(&mut self) -> Option<std::io::Result<AnyBlock>> {
        let mut kv = HashMap::new();

        while let Some(line) = self.iter.next() {
            //dbg!(&line);
            if let Err(e) = line {
                return Some(Err(e));
            }
            let line_orig = line.unwrap();
            let line = line_orig.splitn(2, '#').next().unwrap().trim();
            //dbg!(&line);
            if line.is_empty() {
                if kv.is_empty() {
                    continue;
                } else {
                    break;
                }
            }
            let kwds: Vec<_> = line.splitn(2, ':').collect();
            //            dbg!(&kwds);
            if kwds.len() != 2 {
                return None; // XXXX error
            }
            kv.insert(kwds[0].trim().to_string(), kwds[1].trim().to_string());
        }

        //dbg!(&kv);

        if kv.is_empty() {
            return None;
        }

        let net = if let Some(net) = kv.get("net") {
            net.parse().unwrap() //XXXX
        } else {
            return Some(Ok(AnyBlock::NotNet));
        };

        let cc = if let Some(country) = kv.get("country") {
            assert!(country.as_bytes().len() == 2);
            country.as_bytes()[0..2].try_into().unwrap()
        } else {
            return Some(Ok(AnyBlock::NotNet));
        };

        fn is_true(v: Option<&String>) -> bool {
            match v {
                Some(s) => s == "true",
                None => false,
            }
        }

        let is_anon_proxy = is_true(kv.get("is-anonymous-proxy"));
        let is_anycast = is_true(kv.get("is-anycast-proxy"));
        let is_satellite = is_true(kv.get("is-satellite-provider"));

        Some(Ok(AnyBlock::NetBlock(NetBlock {
            net,
            cc,
            is_anon_proxy,
            is_anycast,
            is_satellite,
        })))
    }
}

impl<I> Iterator for BlockReader<I>
where
    I: Iterator<Item = std::io::Result<String>>,
{
    type Item = NetBlock;
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.get_block() {
                None => return None,
                Some(Err(_)) => return None,
                Some(Ok(AnyBlock::NotNet)) => continue,
                Some(Ok(AnyBlock::NetBlock(n))) => return Some(n),
            }
        }
    }
}
