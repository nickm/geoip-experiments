
use std::path::{Path,PathBuf};
use std::fs::File;
use std::io::{BufReader,BufRead};
use std::convert::TryInto;
use std::str::FromStr;
use std::net::{Ipv4Addr,Ipv6Addr,SocketAddrV6};

use anyhow::Result;
use rangemap::{RangeInclusiveMap,StepLite};

type CCode = [u8;2];

#[derive(Copy,Clone,Ord,PartialOrd,Eq,PartialEq,Debug)]
struct Ip4(Ipv4Addr);

impl From<Ipv4Addr> for Ip4 {
    fn from(a: Ipv4Addr) -> Self{
        Ip4(a)
    }
}
impl From<u32> for Ip4 {
    fn from(a: u32) -> Self {
        Ip4(a.into())
    }
}

impl FromStr for Ip4 {
    type Err=std::num::ParseIntError;
    fn from_str(s: &str) -> std::result::Result<Ip4,Self::Err> {
        let u : u32 = s.parse()?;
        Ok(u.into())
    }
}
impl StepLite for Ip4 {
    fn add_one(&self) -> Self {
       let v:u32 = self.0.into();
        (v+1).into()
    }
    fn sub_one(&self) -> Self {
        let v:u32 = self.0.into();
        (v-1).into()
    }
}

#[derive(Copy,Clone,Ord,PartialOrd,Eq,PartialEq,Debug)]
struct Ip6(Ipv6Addr);

impl From<Ipv6Addr> for Ip6 {
    fn from(a: Ipv6Addr) -> Self{
        Ip6(a)
    }
}
impl From<u128> for Ip6 {
    fn from(a: u128) -> Self {
        Ip6(a.into())
    }
}
impl From<Ip6> for u128 {
    fn from(a: Ip6) -> u128 {
        a.0.into()
    }
}

impl FromStr for Ip6 {
    type Err=std::net::AddrParseError;
    fn from_str(s: &str) -> std::result::Result<Ip6,Self::Err> {
        let u : Ipv6Addr = s.parse()?;
        Ok(u.into())
    }
}
impl StepLite for Ip6 {
    fn add_one(&self) -> Self {
       let v:u128 = self.0.into();
        (v+1).into()
    }
    fn sub_one(&self) -> Self {
        let v:u128 = self.0.into();
        (v-1).into()
    }
}



fn read_db<P,A>(fname: P) -> Result<RangeInclusiveMap<A,CCode>>
    where P:AsRef<Path>,
          A:Ord+Clone+StepLite+FromStr,
          <A as FromStr>::Err: Send+Sync+std::error::Error+'static
{
    let f = File::open(fname.as_ref())?;
    let f = BufReader::new(f);

    let mut map = RangeInclusiveMap::new();
    for line_r in f.lines() {
        let line = line_r?;
        if line.starts_with("#") {
            continue;
        }
        let elts:Vec<_> = line.trim().splitn(3, ',').collect();
        let lo = elts[0].parse()?;
        let hi = elts[1].parse()?;
        let cc = elts[2].as_bytes().get(0..2).unwrap_or(b"XX")
            .try_into()?;

        map.insert(lo..=hi, cc);
    }

    Ok(map)
}

struct Relay {
    name: String,
    fp: [u8;20],
    v4: Ipv4Addr,
    v6: Option<Ipv6Addr>,
}

fn read_relays<P>(fname: P) -> Result<Vec<Relay>>
where P:AsRef<Path>
{
    // XXXX: It would be cleaner to use an iterator but I don't care right now

    let f = File::open(fname.as_ref())?;
    let f = BufReader::new(f);

    let mut v = Vec::new();
    for line_r in f.lines() {
        let line = line_r?;
        if line.starts_with("r ") {
            let elts:Vec<_> = line.trim().split(' ').collect();
            let name = elts[1].to_string();
            let fp = base64::decode_config(elts[2], base64::STANDARD_NO_PAD)?
            [0..20].try_into()?;
            let v4 = elts[5].parse()?;
            v.push(Relay{
                name, fp, v4, v6: None
            })
        } else if line.starts_with("a ") {
            let elts:Vec<_> = line.trim().split(' ').collect();
            let sa:SocketAddrV6 = elts[1].parse()?;
            v.last_mut().unwrap().v6 = Some(sa.ip().clone());
        }
    }

    Ok(v)
}

#[derive(Debug,Clone)]
struct DbSet {
    name: String,
    v4: RangeInclusiveMap<Ip4,CCode>,
    v6: RangeInclusiveMap<Ip6,CCode>,
}

fn load_db_set(dirname: &str) -> Result<DbSet> {
    let mut p = PathBuf::new();
    p.push("..");
    p.push(dirname);
    p.push("geoip");

    let v4 = read_db(&p)?;
    p.pop();
    p.push("geoip6");
    let v6 = read_db(&p)?;

    Ok(DbSet{
        name: dirname.to_string(),
        v4,
        v6
    })
}

fn evaluate(relays: &[Relay], dbs: &[DbSet]) -> Result<()> {
    print!("Name,Fp,Ipv4,Ipv6");
    for db in dbs.iter() {
        print!(",{0} ipv4,{0} ipv6", db.name);
    }
    println!("");
    for r in relays {
        print!("{},{},{},{}",
               r.name,
               hex::encode(r.fp),
               r.v4,
               r.v6.map(|a| a.to_string()).unwrap_or("".to_string()));

        for db in dbs.iter() {
            let cc_v4 = db.v4.get(&r.v4.into()).unwrap_or(b"??".into());
            let cc_v4 = std::str::from_utf8(cc_v4)?;
            let cc_v6 = if let Some(a6) = r.v6 {
                let cc_v6 = db.v6.get(&a6.into()).unwrap_or(b"??".into());
                std::str::from_utf8(cc_v6)?
            } else {
                ""
            };
            print!(",{},{}", cc_v4, cc_v6);
        }
        println!("");
    }

    Ok(())
}

fn main() -> Result<()> {
    let relays = read_relays("/home/nickm/.tor/cached-microdesc-consensus")?;

    let mut v = Vec::new();
    v.push(load_db_set("ipfire")?);
    v.push(load_db_set("iptoasn")?);
    v.push(load_db_set("maxmind-free")?);
    v.push(load_db_set("maxmind-latest")?);
    v.push(load_db_set("maxmind-latest")?);
    v.push(load_db_set("apnic")?);

    evaluate(&relays[..], &v[..])?;

    Ok(())
}
